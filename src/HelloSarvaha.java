import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HelloSarvaha extends HttpServlet
{
	private String message;
	int i=0;
	@Override
	public void init() throws ServletException 
	{
		message="Welcome to Sarvaha Pvt Ltd";

	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException 
	{
		resp.setContentType("text/html");
		PrintWriter pw=resp.getWriter();
		pw.println("<h1>"+message+"</h1>");
		pw.println("<h1>Website VisitCount"+(i++)+"</h1>");
	}
	@Override
	public void destroy() 
	{
	
	}

}
